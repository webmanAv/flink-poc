package com.example.flinkpoc;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PropertyChangedEvent{
    private String entityId;
    private String propertyName;
    private String propertyValue;
}