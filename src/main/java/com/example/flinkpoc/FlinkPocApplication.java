package com.example.flinkpoc;

import com.google.common.base.Strings;
import lombok.Data;
import org.apache.flink.api.common.serialization.DeserializationSchema;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.streaming.connectors.pulsar.PulsarSourceBuilder;
import org.apache.flink.util.Collector;
import org.apache.pulsar.client.api.SubscriptionType;
import org.apache.pulsar.client.impl.conf.ConsumerConfigurationData;

import java.io.IOException;
import java.util.Map;

public class FlinkPocApplication {
	public static final String MAC = "mac";
	public static final String IPV6 = "ipv6";
	public static final String IPV4 = "ipv4";
	public static final String UNKNOWN = "unknown";
	static String pulsarUrl = System.getenv("PULSAR_BROKER_URL");
	static String pulsarAuthPlugin = System.getenv("PULSAR_AUTH_PLUGIN");
	static String pulsarAuthParams = System.getenv("PULSAR_AUTH_PARAMS");
	static String sourceTopic = System.getenv("SOURCE_TOPIC");

	public static void main(String[] args) throws Exception {
		new FlinkPocApplication().run();
	}

	private void run() throws Exception {
		StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
		env.addSource(createConsumer(sourceTopic, new MapDeserializationSchema()))
		.keyBy(x-> {
					if (x.containsKey(MAC)) {
						return new Tuple2<>(MAC, x.get(MAC).toString());
					} else if (x.containsKey(IPV6)) {
						return new Tuple2<>(IPV6, x.get(IPV6).toString());
					} else if (x.containsKey(IPV4)){
						return new Tuple2<>(IPV4, x.get(IPV4).toString());
					}
					return new Tuple2<>(UNKNOWN, UNKNOWN);
				}
				, TypeInformation.of(new TypeHint<Tuple2<String, String>>(){})).process(new KeysHandler());
		env.execute("Flink POC");
	}

	@Data
	static class KeyState{
		private Tuple2<String, String> key;
		private Integer count = 0;
	}

	static class KeysHandler extends KeyedProcessFunction<Tuple2<String, String>, Map<String, Object>, Map<String, Object>> {
		ValueState<KeyState> keyState;
		@Override
		public void open(Configuration parameters) {
			keyState = getRuntimeContext().getState(new ValueStateDescriptor<>("messageCountState", KeyState.class));
		}

		@Override
		public void processElement(Map<String, Object> value, Context ctx, Collector<Map<String, Object>> out) {
			try {
				KeyState state = keyState.value();
				if (state  == null){
					state = new KeyState();
				}

				if (state.getKey() == null){
					state.setKey(new Tuple2<>(ctx.getCurrentKey().f0, ctx.getCurrentKey().f1));
				}
				state.setCount(state.getCount() + 1);
				keyState.update(state);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static<T> SourceFunction<T> createConsumer(String topic, DeserializationSchema<T> deserializationSchema) throws org.apache.pulsar.client.api.PulsarClientException {
		ConsumerConfigurationData consumerConfigurationData = new ConsumerConfigurationData();
		consumerConfigurationData.setSubscriptionType(SubscriptionType.Shared);
		PulsarSourceBuilder<T> builder = PulsarSourceBuilder
				.builder(deserializationSchema)
				.pulsarAllConsumerConf(consumerConfigurationData)
				.serviceUrl(pulsarUrl)
				.topic(topic).subscriptionName(topic);
		if (!Strings.isNullOrEmpty(pulsarAuthPlugin)) {
			builder.authentication(pulsarAuthPlugin, pulsarAuthParams);
		}

		return builder.build();
	}
}
