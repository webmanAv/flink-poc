package com.example.flinkpoc;

import org.apache.flink.api.common.serialization.AbstractDeserializationSchema;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.core.type.TypeReference;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Map;

public class MapDeserializationSchema extends AbstractDeserializationSchema<Map<String, Object>> {
    @Override
    public Map<String, Object> deserialize(byte[] message) throws IOException {
        return new ObjectMapper().readValue(new String(message), new TypeReference<Map<String, Object>>() {});
    }
}